//
//  main.m
//  Low
//
//  Created by Izzy Fraimow on 11/28/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LowThreadTimer.h"

int main (int argc, const char * argv[])
{

	@autoreleasepool {
		NSMutableArray *values = [NSMutableArray array];
		
		for(int index = 1; index < argc; ++index) {
			NSNumber *value = [NSNumber numberWithInt:atoi(argv[index])];
			[values addObject:value];
			NSLog(@"%s", argv[index]);
		}
		
		NSArray *results = [LowThreadTimer timeValues:values maxCount:4];
		
		NSUInteger result = 0;
		for(NSNumber *number in results) {
			NSLog(@"-> %@", number);
			result += [number unsignedIntValue];
		}
		
		NSLog(@"\n\n Total (cumulative) runtime: %lu", result);
	}
	
    return 0;
}

