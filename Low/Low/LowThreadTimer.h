//
//  LowThreadTimer.h
//  Low
//
//  Created by Izzy Fraimow on 11/28/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LowThreadTimer : NSObject

+ (NSArray *)timeValues:(NSArray *)threadValues maxCount:(NSUInteger)maxCount;

@end
