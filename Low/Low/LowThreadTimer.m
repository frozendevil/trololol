//
//  LowThreadTimer.m
//  Low
//
//  Created by Izzy Fraimow on 11/28/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "LowThreadTimer.h"

@implementation LowThreadTimer

+ (NSArray *)timeValues:(NSArray *)threadValues maxCount:(NSUInteger)maxCount; {
	dispatch_queue_t valueQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
	
	__block NSMutableArray *results = [NSMutableArray array];
	NSCondition *resultsLock = [NSCondition new];
	[threadValues enumerateObjectsUsingBlock:^(NSNumber *value, NSUInteger index, BOOL *stop) {
		dispatch_async(valueQueue, ^{
			sleep([value unsignedIntValue]);
			[resultsLock lock]; 
			{
				if([results count] < maxCount)
					[results addObject:value];
			}
			[resultsLock broadcast];
			[resultsLock unlock];
		});
	}];
	
	while([results count] < maxCount) {
		[resultsLock lock];
		[resultsLock wait];
		[resultsLock unlock];
	}
	
	return results;
}

@end
