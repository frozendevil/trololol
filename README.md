**Trololol**
==========
*My new hobby: replying to blatant homework posts on forums with WTF-worthy code*

**Low**
----

In response to [How would I go about programming this math equation?](https://devforums.apple.com/message/585803). Quoted here for posterity:

    What i want it to do:  Have a UILabel display the SUM of the 4 LOWEST of 6 values.
    
    Example: a,b,c,d,e,f.  Their values will be entered by the user, so their values are unknown.
    
    I'm thinking i would need to to an "If Then" statement and some < and > stuff.  How could I go 
    about setting this up?  OR find the sum of all 6 and subtract the 2 highest. I don't know.
    
    - Thanks in advance!

    Message was edited by Invader11 on 11/28/11 at 3:06 PM

<hr>